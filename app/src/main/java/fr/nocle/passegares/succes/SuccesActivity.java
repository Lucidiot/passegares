package fr.nocle.passegares.succes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import fr.nocle.passegares.R;
import fr.nocle.passegares.controlleur.GareCtrl;
import fr.nocle.passegares.controlleur.TamponCtrl;

public class SuccesActivity extends AppCompatActivity {
    private SuccesCtrl succesControlleur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_succes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        succesControlleur = new SuccesCtrl(this);

        //On récupère la liste des succès
        ArrayList<Succes> listeSucces = succesControlleur.getAllSucces();

        //On récupère la progression
        for(Succes s : listeSucces)
        {
            if(s.isEstAffiche() && !s.isEstValide())
            {
                switch(s.getType())
                {
                    case SuccesManager.TypeGare:
                        GareCtrl gareControlleur = new GareCtrl(this);
                        s.setProgression(gareControlleur.getNbGaresTamponnees());
                        gareControlleur.close();
                        break;
                    case SuccesManager.TypeValidation:
                        TamponCtrl tamponControlleur = new TamponCtrl(this);
                        s.setProgression(tamponControlleur.getMaxTamponInGare());
                        tamponControlleur.close();
                        break;
                }
            }
        }

        SuccesAdapter adapter = new SuccesAdapter(this, listeSucces);
        ListView listeSuccesView = (ListView) findViewById(R.id.listeSucces);
        listeSuccesView.setAdapter(adapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
