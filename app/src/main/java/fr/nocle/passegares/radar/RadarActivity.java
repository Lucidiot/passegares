package fr.nocle.passegares.radar;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import fr.nocle.passegares.AjoutRegionActivity;
import fr.nocle.passegares.BuildConfig;
import fr.nocle.passegares.LocationService;
import fr.nocle.passegares.MonnaieActivity;
import fr.nocle.passegares.preferences.PreferencesActivity;
import fr.nocle.passegares.R;
import fr.nocle.passegares.visa.ResumeVisaActivity;
import fr.nocle.passegares.controlleur.InventaireCtrl;
import fr.nocle.passegares.succes.SuccesActivity;

public class RadarActivity extends AppCompatActivity {

    private static final int DEMANDE_DROIT_LOCALISATION = 1;
    public static final String PREFERENCE_ACTIVER_NOTIFICATION = "activerNotification";
    public static final String PREFERENCE_PRECEDENTE_VERSION = "derniereVersionMaJDialogue";
    public static final String PREFERENCE_PREMIER_LANCEMENT = "premierLancement";
    private Intent serviceLocation;
    private boolean serviceEnCours = false;
    private InventaireCtrl inventaireCtrl;

    public Handler messageHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radar);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean premierLancement = preferences.getBoolean(RadarActivity.PREFERENCE_PREMIER_LANCEMENT, true);
        int lastVersionUpdate = preferences.getInt(RadarActivity.PREFERENCE_PRECEDENTE_VERSION, 0);

        if(premierLancement && lastVersionUpdate == BuildConfig.VERSION_CODE) //C'est juste une montée de version, on va mettre premier lancement à false
        {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(PREFERENCE_PREMIER_LANCEMENT, false);
            editor.apply();
            premierLancement = false;
        }

        if(premierLancement)
        {
            Intent i = new Intent(this, AjoutRegionActivity.class);
            i.putExtra("INSTALLATION", true);
            startActivityForResult(i, 0);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(PREFERENCE_PREMIER_LANCEMENT, false);
            editor.putInt(PREFERENCE_PRECEDENTE_VERSION, BuildConfig.VERSION_CODE); //On désactive en même temps la popup des nouveautés
            editor.apply();
        } else {
            checkDisplayUpdateDialog();
        }

        TextView monTexte = (TextView) findViewById(R.id.garePlusProcheNom);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            monTexte.setText(R.string.localisationImpossible);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {

                    monTexte.setText(R.string.localisationEnCours);
                    messageHandler = new MessageHandler(this);
                    Log.d("LOCPG", "Demarrage du service");
                    serviceLocation = new Intent(this, LocationService.class);
                    serviceLocation.putExtra("MESSAGER", new Messenger(messageHandler));
                    serviceLocation.setAction("START");
                }
            } else {
                monTexte.setText(R.string.localisationEnCours);
                messageHandler = new MessageHandler(this);
                Log.d("LOCPG", "Demarrage du service");
                serviceLocation = new Intent(this, LocationService.class);
                serviceLocation.putExtra("MESSAGER", new Messenger(messageHandler));
                serviceLocation.setAction("START");
            }
        }

        inventaireCtrl = new InventaireCtrl(this);
    }

    private void checkDisplayUpdateDialog() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int lastVersionUpdate = preferences.getInt(RadarActivity.PREFERENCE_PRECEDENTE_VERSION, 0);
        if(lastVersionUpdate != BuildConfig.VERSION_CODE)
        {
            //On affiche la boîte de dialogue de mise à jour
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Version 1.3.2\n" +
                    "\n" +
                    "- Ajout d'un système de succès\n" +
                    "- Affichage des tickets fournis et nécéssaire dans la liste des gares\n" +
                    "- Ajout du métro de Lille\n" +
                    "- Ajout du logo ter\n" +
                    "- Nombreuses corrections de données")
                    .setTitle(R.string.dialogMiseAJourTitle);
            /*
            "Version 1.3.1\n" +
                    "\n" +
                    "- Les boutiques sont payantes et ont été réinitialisée\n" +
                    "- Correction de soucis d'affichage de distances\n" +
                    "- Correction de boutons qui restaient présent à l'écran sans raison\n" +
                    "\n" +
                    "Version 1.3.0\n" +
                    "\n" +
                    "- Ajout de boutiques\n" +
                    "- Ajout de la possibilité d'augmenter sa limite de tickets\n" +
                    "- Amélioration de l'affichage des gares\n" +
                    "- Optimisation du stockage du dernier tampon\n" +
                    "- Meilleur affichage des noms\n" +
                    "- Corrections de données en Île-de-France\n"
             */
            dialog.setPositiveButton(R.string.boutonDAccord, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            dialog.create();
            dialog.show();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(PREFERENCE_PRECEDENTE_VERSION, BuildConfig.VERSION_CODE);
            editor.apply();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        if(serviceLocation == null) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, DEMANDE_DROIT_LOCALISATION);
        } else {
            if(!serviceEnCours) {
                startService(serviceLocation);
                serviceEnCours = true;
            }

            Message message = Message.obtain();
            message.arg1 = 2;
            messageHandler.sendMessage(message);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(serviceLocation != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            boolean notification = preferences.getBoolean(RadarActivity.PREFERENCE_ACTIVER_NOTIFICATION, true);
            if (notification) {
                Message message = Message.obtain();
                message.arg1 = 1;
                messageHandler.sendMessage(message);
            } else if (serviceEnCours) {
                stopService(serviceLocation);
                serviceEnCours = false;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Message message = Message.obtain();
        message.arg1 = 2;
        messageHandler.sendMessage(message);
        stopService(serviceLocation);
        inventaireCtrl.close();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case DEMANDE_DROIT_LOCALISATION: {
                // If request is cancelled, the result arrays are empty.
                TextView monTexte = (TextView) findViewById(R.id.garePlusProcheNom);
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Bon, bah, on demande la localisation
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    {
                        return;
                    }

                    monTexte.setText(R.string.localisationEnCours);
                    messageHandler = new MessageHandler(this);
                    serviceLocation = new Intent(this, LocationService.class);
                    serviceLocation.putExtra("MESSAGER", new Messenger(messageHandler));
                    serviceLocation.setAction("START");
                } else {
                    monTexte.setText(R.string.localisationImpossible);
                    return;
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_radar, menu);
        MenuItem menuItem = menu.findItem(R.id.voirTickets).setTitle(String.valueOf(inventaireCtrl.getTotalTicket()));

        menuItem.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case R.id.voirTampons:
                i = new Intent(getApplicationContext(), ResumeVisaActivity.class);
                startActivity(i);
                return true;
            case R.id.voirTamponsDuJour:
                i = new Intent(getApplicationContext(), ResumeVisaActivity.class);
                i.putExtra("DUJOUR", true);
                startActivity(i);
                return true;
            case R.id.voirSucces:
                i = new Intent(getApplicationContext(), SuccesActivity.class);
                startActivity(i);
                return true;
            case R.id.preferencesItem:
                i = new Intent(getApplicationContext(), PreferencesActivity.class);
                startActivity(i);
                return true;
            case R.id.voirTickets:
                i = new Intent(getApplicationContext(), MonnaieActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
