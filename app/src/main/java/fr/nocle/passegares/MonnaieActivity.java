package fr.nocle.passegares;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import fr.nocle.passegares.adapter.MonnaieAdapter;
import fr.nocle.passegares.controlleur.InventaireCtrl;
import fr.nocle.passegares.modele.Ticket;

public class MonnaieActivity extends AppCompatActivity {

    private InventaireCtrl inventaireCtrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monnaie);

        setTitle(R.string.porteTicket);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inventaireCtrl = new InventaireCtrl(this);

        final ArrayList<Ticket> listeTicket = inventaireCtrl.getListTicket();

        int nbTotalTicket = 0;
        for(Ticket t : listeTicket)
            nbTotalTicket += t.getNombre();

        TextView champ = (TextView) findViewById(R.id.nombreTicket);
        champ.setText(getString(R.string.nombreTicket) + " " + nbTotalTicket + "/" + inventaireCtrl.getLimiteTicket());

        MonnaieAdapter monnaieAdapter = new MonnaieAdapter(this, listeTicket);

        final GridView listeView = (GridView) this.findViewById(R.id.listeTicket);
        listeView.setAdapter(monnaieAdapter);

        listeView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final Ticket t = listeTicket.get(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(MonnaieActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.jeter_ticket_picker, null))
                        // Add action buttons
                        .setPositiveButton(R.string.boutonJeter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                AlertDialog ad = (AlertDialog) dialog;
                                NumberPicker numberPicker = (NumberPicker) ad.findViewById(R.id.jeterTicketNombre);

                                int nbTicket = numberPicker.getValue();
                                inventaireCtrl.jeterTicket(t.getIdentifiant(), nbTicket);

                                t.setNombre(t.getNombre() - nbTicket);

                                listeView.invalidateViews();
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(R.string.boutonAnnuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.show();

                NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.jeterTicketNombre);
                numberPicker.setMinValue(0);
                numberPicker.setMaxValue(t.getNombre());
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        inventaireCtrl.close();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
