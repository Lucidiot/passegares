package fr.nocle.passegares.preferences;

import android.content.Intent;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.os.Bundle;

import fr.nocle.passegares.AjoutRegionActivity;
import fr.nocle.passegares.BuildConfig;
import fr.nocle.passegares.CreditsActivity;
import fr.nocle.passegares.R;

public class PreferencesActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            Preference versionAPKPref = (Preference) findPreference("versionAPK");
            String versionName = BuildConfig.VERSION_NAME;
            versionAPKPref.setSummary(versionName);

            Preference iconeApplication = (Preference) findPreference("iconeCredits");
            iconeApplication.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(getActivity(), CreditsActivity.class);

                    startActivity(i);
                    return true;
                }
            });

            Preference ajoutRegion = (Preference) findPreference("ajoutRegion");
            ajoutRegion.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(getActivity(), AjoutRegionActivity.class);

                    startActivity(i);
                    return true;
                }
            });

            Preference preferencesAvancees = (Preference) findPreference("preferencesAvancees");
            preferencesAvancees.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(getActivity(), PreferencesAvanceesActivity.class);

                    startActivity(i);
                    return true;
                }
            });
        }
    }
}
