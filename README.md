Version en développement : [![build status](https://framagit.org/JonathanMM/passegares/badges/master/build.svg)](https://framagit.org/JonathanMM/passegares/commits/master)

Version stable (présente sur le play store) : [![build status](https://framagit.org/JonathanMM/passegares/badges/stable/build.svg)](https://framagit.org/JonathanMM/passegares/commits/stable)

À récupérer sur les stores :
- Play Store : https://play.google.com/store/apps/details?id=fr.nocle.passegares
- F-droid : https://f-droid.org/repository/browse/?fdfilter=passegares&fdid=fr.nocle.passegares

Présentation
============

PasseGares est une application android ayant pour contexte les gares et stations
de transport en communs. Il permet de lister l'ensemble des gares auquel on est 
passé, en les tamponnant quand on se trouve à moins de 150m d'elles.

Une partie jeu de gestion est en cours de création autour de ce concept.

Elle fonctionne qu'avec les gares de la France pour le moment. Néanmoins, vous 
pouvez aider à l'intégration de données pour d'autres pays. Vous pouvez également
contribuer en ajoutant les données sur les lignes TER, en corrigeant des fautes
d'orthographes, en traduisant l'application. Tous les détails sont disponible
dans le fichier CONTRIBUTING.md

Installation
============

Cette application est compilé avec android-studio.

Contribution
============

Cette application est ouverte aux contributions.

Crédits
=======

Les logos et sources de données appartiennent à leur propriétaire respectifs, et
ne sont pas compris dans le cadre de la licence de ce logiciel. Vous pouvez les
retrouver dans le fichier Preference.java (le code est toujours à jour :) ).

Contact
=======

Vous pouvez poster une requête de bug, ou envoyer un courriel à :
passegares[@]nocle.fr