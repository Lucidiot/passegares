#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml
import os,sys
import urllib.request

tmpFileName = "data.csv"
entetesDonnees = ['idExterne','nom','exploitant','latitude','longitude','couleur','couleurEvolution','vCreation','vMaj','vSuppression']
regionsFichier = {}
metadonnes = 123
prochaineCouleur = -1
seulementUnFichier = "mel_lille_metro.yml"

def getCouleur():
  global prochaineCouleur
  prochaineCouleur += 1
  if prochaineCouleur >= 8:
    prochaineCouleur %= 8
  return prochaineCouleur

def getCouleurEvolution(nom, couleur):
  couleurEvolution = (couleur + len(nom)) % 8
  if couleurEvolution == couleur:
    couleurEvolution = (couleur + 3) % 8
  return couleurEvolution

def getEntetes(ligne, correspondancesCol = None):
    posCol = {}
    champ = ligne.strip().split(';')
    i = 0
    for c in champ:
        if correspondancesCol == None :
            posCol[c] = i
        elif c in correspondancesCol.keys():
            posCol[correspondancesCol[c]] = i
        i += 1
    return posCol

def nextIdRegion(fRegions):
  idMaxRegion = 0
  i = 0
  posIdRegion = 0
  for ligne in fRegions.readlines():
    contenu = ligne.strip().split(';')
    if i == 0: #entête
      j = 0
      for c in contenu:
        if c == 'id':
          posIdRegion = j
          break
        j += 1
    else:
      if idMaxRegion < int(contenu[posIdRegion]):
        idMaxRegion = int(contenu[posIdRegion])
    i += 1
  return idMaxRegion + 1

def getAllIdExternes(fichier):
  idExternes = []
  idExterneKey = 'idExterne'
  i = 0
  posId = 0
  for ligne in fichier.readlines():
    contenu = ligne.strip().split(';')
    if i == 0: #entête
      j = 0
      for c in contenu:
        if c == idExterneKey:
          posId = j
          break
        j += 1
    else:
      idExternes.append(contenu[posId])
    i += 1
  return idExternes

def getIdRegions(fichier):
  idExterneKey = 'dossierId'
  idExternes = {}
  entetes = {}
  i = 0
  posIdExterne = 0
  lignes = fichier.readlines() #On doit récupérer toutes les lignes pour pouvoir les écrire ensuite
  for ligne in lignes:
    contenu = ligne.strip().split(';')
    if i == 0: #entête
      j = 0
      for c in contenu:
        if c == idExterneKey:
          posIdExterne = j
        entetes[j] = c
        j += 1
    else:
      idExternes[contenu[posIdExterne]] = i
    i += 1
  return {'idExternes': idExternes, 'lignes': lignes, 'entetes': entetes}

def getDataLigneRegion(ligne, entetes):
  dataContenu = {}
  contenu = ligne.strip().split(';')
  i = 0
  for c in contenu:
    dataContenu[entetes[i]] = c
    i += 1
  return dataContenu

def getFichierRegion(gare, config):
    global metadonnes
    regionGare = gare['dossierId']
    if not regionGare in regionsFichier.keys(): #Pas encore en cache
        #On regarde si le dossier existe ou pas
        regionPath = '../app/src/main/assets/'+regionGare+'/'
        if not os.path.isdir(regionPath):
            #On crée le dossier
            os.mkdir(regionPath)
            #On crée les fichiers
            regionsFichier[regionGare] = {
                'gares': open(regionPath+'Gares.csv', 'a+'),
                'lignes': open(regionPath+'Lignes.csv', 'a'),
                'gdl': open(regionPath+'GaresDansLigne.csv', 'a'),
                'idGares': [],
                'idLignes': [],
                'garesDansLigneAssociation' : {}
            }
            #Et on va leur mettre leur entêtes
            regionsFichier[regionGare]['gares'].write("idExterne;nom;exploitant;latitude;longitude;couleur;couleurEvolution;vCreation;vMaj;vSuppression\n")
            regionsFichier[regionGare]['lignes'].write("idExterne;nom;type;ordre;couleur;vCreation;vMaj;vSuppression\n")
            regionsFichier[regionGare]['gdl'].write("idGare;idLigne;ordre;PDLFond;PDLPoint;vCreation;vMaj;vSuppression\n")
        else:
            #On récupère les trois fichiers
            regionsFichier[regionGare] = {
                'gares': open(regionPath+'Gares.csv', 'r+'),
                'lignes': open(regionPath+'Lignes.csv', 'r+'),
                'gdl': open(regionPath+'GaresDansLigne.csv', 'a'),
                'garesDansLigneAssociation' : {}
            }
            #Et on génère le tableau idGares et idLignes
            regionsFichier[regionGare]['idGares'] = getAllIdExternes(regionsFichier[regionGare]['gares'])
            regionsFichier[regionGare]['idLignes'] = getAllIdExternes(regionsFichier[regionGare]['lignes'])
        
        if config['substitue'] != None and 'dossierId' in config['substitue'] and regionGare in config['substitue']['dossierId'].keys():
          regionGareAlt = config['substitue']['dossierId'][regionGare]
        else:
          regionGareAlt = regionGare
            
        #Si ligne unique, on la crée a ce moment
        if not config['lignes']:
          idLigneUnique = config['prefixIdExterne']+"_"+regionGareAlt+"_U"
          if not idLigneUnique in regionsFichier[regionGare]['idLignes']:
            regionsFichier[regionGare]['lignes'].write(idLigneUnique+ ";Ligne Unique;"+config['lignesType']+";0;#000000;"+str(metadonnes)+";0;0\n")
            regionsFichier[regionGareAlt]['idLignes'].append(idLigneUnique)
        
        #On va préparer l'objet d'association Gare et Ligne
        for idLigne in regionsFichier[regionGareAlt]['idLignes']:
          regionsFichier[regionGareAlt]['garesDansLigneAssociation'][idLigne] = []
        
        #On rempli également le fichier des régions
        fRegions = open('../app/src/main/assets/Regions.csv', 'r+')
        dataRegions = getIdRegions(fRegions)
        idRegions = dataRegions['idExternes']
        if not gare['region'] in idRegions.keys():
          #Il nous faut l'id max (dernière région)
          idNewRegion = nextIdRegion(fRegions)
          fRegions.write(str(idNewRegion)+";"+gare['region']+";"+regionGare+";"+str(metadonnes)+";0\n")
        else:
          numLigne = idRegions[gare['region']]
          lignes = dataRegions['lignes']
          dataLigne = getDataLigneRegion(lignes[numLigne], dataRegions['entetes'])
          lignes[numLigne] = str(dataLigne['id'])+";"+dataLigne['nom']+";"+dataLigne['dossierId']+";"+str(dataLigne['vCreation'])+";"+str(metadonnes)+"\n"
          #On nettoie le fichier
          fRegions.seek(0, 0)
          fRegions.truncate()
          #Et on écrit
          fRegions.writelines(lignes)
        fRegions.close()
        
        #On récupère également les entêtes
        regionsFichier[regionGare]['gares'].seek(0, 0)
        regionsFichier[regionGare]['enteteGare'] = getEntetes(regionsFichier[regionGare]['gares'].readline())
        #Et on remet le curseur à sa place
        regionsFichier[regionGare]['gares'].seek(0, 2)
        
    return regionsFichier[regionGare]

def getValue(key, data, defaultValue):
  if not key in gare:
    return defaultValue[key]
  return data[key]

def getCoordonnees(gare):
  if not 'latitude' in gare and not 'longitude' in gare:
    if not 'coordonnees' in gare:
      raise Exception('Aucune coordonnées mentionnées dans le fichier')
    coupe = gare['coordonnees'].split(',')
    gare['latitude'] = coupe[0]
    gare['longitude'] = coupe[1]

#On récupère le fichier de config
for filePath in os.listdir('config/'):
  if seulementUnFichier != None:
    if filePath != seulementUnFichier:
      continue
  fConfig = open("config/"+filePath, 'r+')
  config = yaml.load(fConfig)
  fConfig.close()

  #Étape 1 : Téléchargement du fichier
  lien = config['link_download']
  print('Téléchargement de : ' + lien)
  urllib.request.urlretrieve(lien, tmpFileName) #Désactivation durant les tests
  print('Téléchargement terminé')
  fData = open(tmpFileName, 'r+')

  #Étape 2 : On récupère ses entêtes
  titre = fData.readline()
  posCol = getEntetes(titre, config['header'])
  print('Liste des entêtes : ')
  print(posCol)

  #Étape 3 : On lit le fichier
  for ligne in fData.readlines():
    contenu = ligne.strip().split(';')
    gare = {}
    for c,i in posCol.items():
      gare[c] = contenu[i]
    
    #On va regarder la région
    regionGare = getValue('dossierId', gare, config['default_value'])
    gare['region'] = regionGare
    #A-t-elle une valeur normalisée ?
    if config['substitue'] != None and 'dossierId' in config['substitue'] and regionGare in config['substitue']['dossierId'].keys():
      regionGare = config['substitue']['dossierId'][regionGare]
    gare['dossierId'] = regionGare
        
    if len(regionGare) > 0 and (config['ignore_values'] == None or not regionGare in config['ignore_values']['dossierId']):
      #On ajoute le prefixe à l'Id
      gare['idExterne'] = config['prefixIdExterne']+gare['idExterne']
      
      if config['substitue'] != None and 'idExterne' in config['substitue'] and gare['idExterne'] in config['substitue']['idExterne']:
        gare['idExterne'] = str(config['substitue']['idExterne'][gare['idExterne']])
      #On récupère le fichier de la région
      fRegionInfos = getFichierRegion(gare, config)
      
      #On ne traite que les nouvelles gares
      if not gare['idExterne'] in fRegionInfos['idGares']:
        print('nouvelle gare : ' + gare['nom'])
        fRegionInfos['idGares'].append(gare['idExterne'])
        gare['exploitant'] = getValue('exploitant', gare, config['default_value'])
        #On va récupérer la couleur et la couleur évolution
        gare['couleur'] = getCouleur()
        gare['couleurEvolution'] = getCouleurEvolution(gare['nom'], gare['couleur'])
        #Si on a des coordonnees, on va les traiter
        getCoordonnees(gare)
        #On rempli le fichier Gares
        fRegionInfos['gares'].write(gare['idExterne']+';'+gare['nom']+';'+gare['exploitant']+';'+gare['latitude']+';'+gare['longitude']+';'+str(gare['couleur'])+';'+str(gare['couleurEvolution'])+';'+str(metadonnes)+';0;0'+"\n")
        
      if not config['lignes']: #Ligne unique
        idLigneGare = config['prefixIdExterne']+"_"+regionGare+"_U"
      else:
        idLigneGare = "Ligne_"+config['prefixIdExterne']+gare['ligne']
      
      #On regarde pour la ligne maintenant
      if not idLigneGare in regionsFichier[regionGare]['idLignes']:
        regionsFichier[regionGare]['idLignes'].append(idLigneGare)
        regionsFichier[regionGare]['garesDansLigneAssociation'][idLigneGare] = []
        #On va créer la ligne
        regionsFichier[regionGare]['lignes'].write(idLigneGare+ ";Ligne "+gare['ligne']+";"+config['lignesType']+";0;#000000;"+str(metadonnes)+";0;0\n")
        
      regionsFichier[regionGare]['garesDansLigneAssociation'][idLigneGare].append(gare['idExterne'])
  
  #Étape 4 : Post traitement
  for regionId,regionFichier in regionsFichier.items():
    for idLigne,listeGares in regionFichier['garesDansLigneAssociation'].items():
      #Étape 4b : On rempli le fichier GaresDansLigne
      for idGare in listeGares:
        regionFichier['gdl'].write(idGare+";"+idLigne+";0;0;0;"+str(metadonnes)+";0;0\n")
      
    #Étape 4c : On ferme les fichiers
    regionFichier['gares'].close()
    regionFichier['lignes'].close()
    regionFichier['gdl'].close()
  
  #Étape 5 : On efface le fichier data
  os.remove(tmpFileName)

print('Fin de l\'import')
